<?php
/**
 * User: Tomáš Chroboček
 * Date: 3. 7. 2018
 * Time: 21:59
 */

namespace Qweree\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TestController
 * @package Qweree\Controller
 */
class TestController extends Controller
{
    /**
     * @return Response
     */
    public function defaultAction(): Response
    {
        return new Response('
        <html>
          <body>
            Qweree!
          </body>
        </html>
        ');
    }
}
